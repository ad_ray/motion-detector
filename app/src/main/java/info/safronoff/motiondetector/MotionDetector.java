package info.safronoff.motiondetector;

import android.animation.RectEvaluator;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;

/**
 * Created by adray on 1/19/17.
 */

public class MotionDetector {

    static final long FRAME_DIFF_THRESHOLD = 120000;
    static final long WAIT_TIME = 5000;
    static final long FRAMES_TO_SAVE = 3;
    static final double IMG_THRESHOLD = 20;
    static final int THUMBS_QUEUE_SIZE = 24;
    static final double OBJECT_SIZE = 0.01;
    private final ImageView filter;

    CameraFrame bgFrame;
    CameraFrame prevFrame;
    long frameCount = 0;
    long frames = 0;
    long lastMotion = 0;

    ImageView bg;
    ImageView movingObject;
    ImageView frameToSave;
    List<CameraFrame> framesWithMotion = new ArrayList();

    long motionCount = 0;

    Camera camera;
    Context context;

    private boolean showingFrames = false;

    private Queue<Mat> thumbs = new ArrayDeque<>(THUMBS_QUEUE_SIZE);

    public MotionDetector(Context context, ImageView bg, ImageView movingObject, ImageView frameToSave, Camera camera, ImageView filter) {
        this.bg = bg;
        this.movingObject = movingObject;
        this.camera = camera;
        this.frameToSave = frameToSave;
        this.filter = filter;
        this.context = context;

    }



    public void nextFrame(CameraFrame frame) {

        //filterFrame(frame);

        tryUpdateBG(frame);

        detectMotion(frame);
    }


    private void addThumb(Mat thumb) {
        if(thumbs.size() > THUMBS_QUEUE_SIZE) {
            thumbs.remove();
        }
        thumbs.add(thumb);
    }

    private void tryUpdateBG(CameraFrame inframe) {
        frames ++;
        addThumb(inframe.thumb);
        if(frames > 240 && thumbs.size() >= THUMBS_QUEUE_SIZE) {
            if(frames == 300) camera.autoFocus(null);

            Mat oldFrame = thumbs.remove();
            double diff = inframe.diff(oldFrame);
            //System.out.println(diff);
            if (oldFrame.size().equals(inframe.thumb.size())
                    && diff < FRAME_DIFF_THRESHOLD) {
                frames = 0;
                bgFrame = inframe;
                mat2image(bgFrame.thumb, bg);

            }
        }
    }


    public void detectMotion(CameraFrame frame) {
        if(bgFrame != null && frame.thumb.size().equals(bgFrame.thumb.size()) ) {
            Mat thresh = thresholdMovingObject(frame);
            List<Rect> contours = getContours(thresh);
            frame.setContours(contours);
            for(Rect rect : contours) {
                Imgproc.rectangle(thresh
                        , new Point(rect.x, rect.y)
                        , new Point(rect.x + rect.width, rect.y + rect.height)
                        , new Scalar(255, 255, 255), 3);
            }
            mat2image(thresh, movingObject);
            //record only certain amount of frames, save couple of frames than wait for next movment
            if(!showingFrames) {
                if (framesWithMotion.size() == 30 || (contours.size() == 0 && framesWithMotion.size() > 0)) {
                    saveAndClearFrames();
                    lastMotion = System.currentTimeMillis();
                } else {
                    if (System.currentTimeMillis() - lastMotion > WAIT_TIME && contours.size() > 0) {
                        framesWithMotion.add(frame);
                    }
                }
            }
        }
    }


    private MatAndBG objFromFrame(CameraFrame frame) {
        Rect maxContour = frame.getContours().get(0);
        for(Rect r : frame.getContours()) {
            if(r.height * r.width  > maxContour.height * maxContour.width)
                maxContour = r;
        }

        float xFactor = (float)frame.original.width() / frame.thumb.width();
        float yFactor = (float)frame.original.height() / frame.thumb.height();
        Rect r = new Rect(

                (int)(maxContour.x * xFactor),
                (int)(maxContour.y * yFactor) ,
                (int)(maxContour.width * xFactor),
            (int)(maxContour.height * yFactor)

        );

        return new MatAndBG(frame.original.submat(r), bgFrame.original.submat(r));
    }

    private Mat cutObject(CameraFrame frame) {
        MatAndBG matAndBG = objFromFrame(frame);

        //Imgproc.spatialGradient(obj, obj, );

        Mat delta = new Mat(matAndBG.frame.size(), matAndBG.frame.type());
        Core.absdiff(matAndBG.frame, matAndBG.bg, delta);

        Imgproc.cvtColor(delta, delta, Imgproc.COLOR_BGR2GRAY);

        //Imgproc.dilate(delta, delta, new Mat(), new Point(-1, -1), 2);

        Imgproc.blur(delta, delta, new Size(4, 4));
        Imgproc.threshold(delta, delta, IMG_THRESHOLD, 255, Imgproc.THRESH_BINARY);

        Mat obj = new Mat();
        matAndBG.frame.copyTo(obj, delta);
        return obj;
    }

    private Mat processFrame(CameraFrame frame) {

        Mat edges = new Mat();
        Mat obj = cutObject(frame);
        //Imgproc.threshold(obj, obj, 128, 255, Imgproc.THRESH_BINARY_INV);
        Imgproc.Canny(obj, edges, 200, 300);

        //Imgproc.threshold(edges, edges, IMG_THRESHOLD, 255, Imgproc.THRESH_BINARY);

        return edges;
    }



    private double calcSum(Mat m) {
        double[] sumVector = Core.sumElems(m).val;
        double sum = 0;
        for(double elm : sumVector) {
            sum += elm;
        }
        return sum;
    }

    private void calcGradients() {
        for(CameraFrame frame: framesWithMotion) {
            frame.movingObjGradient = calcSum(processFrame(frame));
        }
    }

    private void sortFrames() {
      Collections.sort(framesWithMotion, new Comparator<CameraFrame>() {
          @Override
          public int compare(CameraFrame o1, CameraFrame o2) {
              return (int) Math.round(o2.movingObjGradient - o1.movingObjGradient);
          }
      });
    }

    private void saveMat(File file, Mat mat) throws IOException {
        Bitmap bm = mat2image(mat);
        FileOutputStream out = new FileOutputStream(file);
        bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
        out.flush();
        out.close();
    }

    public static File getDayDir() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(Calendar.getInstance().getTime());
        File storageDir  = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                ),
                "Motion Detector"
        );
        File dayDir = new File(storageDir, date);
        return dayDir;
    }
    private void saveToFs(CameraFrame bg, CameraFrame... frames) {
        //motionCount ++;


        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String time = timeFormat.format(Calendar.getInstance().getTime());
        File dayDir = getDayDir();
        try {

            dayDir.mkdirs();

            Log.d("SAVING TO", dayDir.getAbsolutePath());
            File bgFile = new File(dayDir, String.format("%s_bg.jpg", time));

            saveMat(bgFile, bg.original);
            for (int i = 0; i < frames.length; i++) {
                File file = new File(dayDir, String.format("%s_%02d.jpg", time,  i));
                saveMat(file, frames[i].original);
                MediaScannerConnection.scanFile(context,
                        new String[] { file.toString() }, null,
                        (MediaScannerConnection.OnScanCompletedListener) (path, uri) -> {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        });
            }


        } catch (Exception e) {
            Log.e("Error saving file", e.getMessage());
        } finally {
            framesWithMotion.clear();
        }
    }

    private void saveAndClearFrames() {
        if(!showingFrames) {
            showingFrames = true;
            AsyncTask findBestFrames = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    calcGradients();
                    sortFrames();
                    frameToSave.post(new Runnable() {
                        @Override
                        public void run() {
                            mat2image(processFrame(framesWithMotion.get(0)), frameToSave);
                            saveToFs(bgFrame, framesWithMotion.get(0));
                            framesWithMotion.clear();
                            showingFrames = false;
                        }
                    });
                    return null;
                }


            };
            findBestFrames.execute(new Object());

        }
    }

    private Mat thresholdMovingObject(CameraFrame frame) {

            Mat delta = new Mat(frame.thumb.size(), frame.thumb.type());
            Core.absdiff(bgFrame.thumb, frame.thumb, delta);
            Mat thresh = new Mat();
            Imgproc.threshold(delta, thresh, IMG_THRESHOLD, 255, Imgproc.THRESH_BINARY);

            Imgproc.dilate(thresh, thresh, new Mat(), new Point(-1, -1), 2);
            return  thresh;


    }

    private List<Rect> getContours(Mat thresh) {
        List<MatOfPoint> contours = new ArrayList<>();
        Rect roi = null;
        if(Prefs.isROIenabled(context)) {
            roi = Prefs.getRoiRect(DrawView.instance);
            roi.x = (int) Math.round(roi.x * 256.0 / 100);
            roi.y = (int) Math.round(roi.y * 256.0 / 100);
            roi.width = (int) Math.round(roi.width * 256.0 / 100);
            roi.height = (int) Math.round(roi.height * 256.0 / 100);
        }
        List<Rect> rects = new ArrayList<>();
        Mat hierachy = new Mat();
        Imgproc.findContours(thresh, contours, hierachy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        for(MatOfPoint ctr: contours) {
            double area = Imgproc.contourArea(ctr);
            //System.out.println(area);
            if(area > 200) {
                Rect rect = Imgproc.boundingRect(ctr);
                if(roi != null && intersects(roi, rect) || roi == null) {
                    float objScale = (1 + Prefs.getObjectSize(context)/100 * 19)/100;
                    if (rect.width > thresh.width() * objScale && rect.height > thresh.height() * objScale)
                        rects.add(rect);
                }

            }
        }

        return rects;
        //mat2image(thresh, movingObject);
    }

    private boolean intersects(Rect a, Rect b) {
        int x = Math.max(a.x, b.x);
        int y = Math.max(a.y, b.y);
        int w = Math.min(a.x+a.width, b.x+b.width) - x;
        int h = Math.min(a.y+a.height, b.y+b.height) - y;
        return !(w<0 || h<0);
    }

    private Bitmap mat2image(Mat m) {
        Bitmap bm = Bitmap.createBitmap(m.cols(), m.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(m, bm);
        return bm;
    }

    private void mat2image(Mat m, final ImageView img) {
        final Bitmap bm = mat2image(m);
        img.post(new Runnable() {
            @Override
            public void run() {
                img.setImageBitmap(bm);
            }
        });
    }
}
