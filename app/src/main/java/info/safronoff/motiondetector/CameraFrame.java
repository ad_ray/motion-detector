package info.safronoff.motiondetector;

import android.content.Context;
import android.hardware.Camera;
import android.view.Display;
import android.view.WindowManager;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;

/**
 * Created by adray on 1/19/17.
 */

public class CameraFrame {
    Mat original;
    Mat thumb;

    double movingObjGradient = 0.0;

    List<Rect> contours;

    public CameraFrame(byte[] data, Camera camera, int orientation) {
        original = previewToMat(data, camera, orientation);
        thumb = calcThumb(original);
    }

    public List<Rect> getContours() {
        return contours;
    }

    public void setContours(List<Rect> contours) {
        this.contours = contours;
    }

    private Mat calcThumb(Mat a) {
        thumb = new Mat();
        Imgproc.resize(a, thumb, new Size(256, 256));
        Imgproc.cvtColor(thumb, thumb, Imgproc.COLOR_BGR2GRAY);
        Imgproc.blur(thumb, thumb, new Size(4, 4));

        return thumb;


    }

    private void rotate(Mat a, int orientation) {


        if(orientation == 0) {
            Core.transpose(a, a);
            Core.flip(a, a, 0);
            Core.flip(a, a, 1);
        } else if(orientation == 1) {
            Core.flip(a, a, 1);
        } else if(orientation == 3 ) {
            Core.flip(a, a, 0);
            //flip(a, a, 1);
        } else if(orientation == 4) {
            Core.transpose(a, a);
            Core.flip(a, a, 1);
        } else if(orientation == 7) {
            Core.flip(a, a, 0);
            Core.flip(a, a, 1);
        }
    }

    public double diff(Mat a) {
        return diff(a, thumb);
    }

    private double diff(Mat a, Mat b) {
        Mat diff = new Mat(a.size(), a.type());
        Core.absdiff(a, b, diff);
        double[] sumVector = Core.sumElems(diff).val;
        double sum = 0;
        for(double elm : sumVector) {
            sum += elm;
        }
        System.out.println(sum);
        return sum;
    }

    private Mat previewToMat(byte[] data, Camera camera, int orientation) {
        final Camera.Size preview = camera.getParameters().getPreviewSize();
        int h = preview.height;
        int w = preview.width;
        //System.out.println(String.format("h: %s, w: %s, bpp: %s", preview.height, preview.width, bpp));
        Mat frame = new Mat(h + h / 2, w, CvType.CV_8UC1);

        //
        frame.put(0, 0, data);
        Imgproc.cvtColor(frame, frame, Imgproc.COLOR_YUV2RGB_NV21);
        rotate(frame, orientation);
        return frame;
    }
}
