package info.safronoff.motiondetector;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    static{ System.loadLibrary("opencv_java3"); }




    private Camera mCamera;
    private CameraPreview mPreview;
    private DrawView roi;



    private CameraBridgeViewBase mOpenCvCameraView;

    public static Camera getCameraInstance(Context context) {
        Camera c = null;
        try {
            int id = 0;

            c = Camera.open(id); // attempt to get a Camera instance


            c.setPreviewCallback(new Camera.PreviewCallback() {
                public void onPreviewFrame(byte[] _data, Camera _camera) {
                    Log.d("Camera", "PREVIEW");
                }
            });
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            Log.e("Camera", e.getMessage());
        }
        return c; // returns null if camera is unavailable
    }

    private void startCapture() {

        startCamera();


    }

    private void startCamera() {
        try {
            mCamera = getCameraInstance(getApplicationContext());
            if (mCamera != null) {
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPreviewSize(640, 480);//TODO parametrize
                mCamera.setParameters(parameters);
                // mCamera.setPreviewCallback(this);

                mPreview = new CameraPreview(this, mCamera);
                //Size cameraSize = getSize(mCamera.getParameters().getPreviewSize());
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_view);
                preview.addView(mPreview);
                mCamera.setPreviewCallback(mPreview);
                mCamera.startPreview();


        }

        } catch (Exception e) {
            Log.e("Camera", e.getMessage());
        }
    }

    private void stopCapture() {
        try {
            // mPreview.stopCapture();
            if (mCamera != null) {
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_view);
                preview.removeAllViews();
                mCamera.stopPreview();
                mCamera.setPreviewDisplay(null);
                mCamera.setPreviewCallback(null);

                mPreview.getHolder().removeCallback(mPreview);
                mCamera.release();
                mCamera.unlock();
            }
        } catch (Exception e) {
            Log.e("Camera", e.getMessage());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        if( ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    777);

        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        View gallery = findViewById(R.id.gallery);
        roi = (DrawView) findViewById(R.id.roi_view);
        CheckBox roiToggle = (CheckBox) findViewById(R.id.roi);


        CheckBox backgroundTOggle = (CheckBox) findViewById(R.id.bg_toggle);
        backgroundTOggle.setChecked(Prefs.isBGEnabled(this));
        View bg = findViewById(R.id.background);
        bg.setVisibility(Prefs.isBGEnabled(this)? View.VISIBLE : View.GONE);
        backgroundTOggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                bg.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                Prefs.setBGEnagled(getApplicationContext(), isChecked);
            }
        });

        CheckBox moveToggle = (CheckBox) findViewById(R.id.moving_thumb_toggle);
        moveToggle.setChecked(Prefs.isMovingEnabled(this));
        View moving = findViewById(R.id.moving_object);
        moving.setVisibility(Prefs.isMovingEnabled(this)? View.VISIBLE : View.GONE);
        moveToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                moving.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                Prefs.setMovingEnagled(getApplicationContext(), isChecked);
            }
        });


        CheckBox contourToggle = (CheckBox) findViewById(R.id.contour_toggle);
        contourToggle.setChecked(Prefs.isMovingEnabled(this));
        View contour = findViewById(R.id.frame_to_save);
        contour.setVisibility(Prefs.isMovingEnabled(this)? View.VISIBLE : View.GONE);
        contourToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                contour.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                Prefs.setContourEnagled(getApplicationContext(), isChecked);
            }
        });


        if(Prefs.isROIenabled(this)) {
            roi.setVisibility(View.VISIBLE);
            roiToggle.setChecked(true);
        } else {
            roi.setVisibility(View.INVISIBLE);
            roiToggle.setChecked(false);
        }
        roi.post(new Runnable() {
            @Override
            public void run() {
                roi.setPoints(Prefs.getScaledRoiData(roi));
            }
        });

        roiToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                roi.post(new Runnable() {
                    @Override
                    public void run() {
                        Prefs.setRoi(getApplicationContext(), isChecked, roi);
                        roi.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
                    }
                });

            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri selectedUri = Uri.parse(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                ) + "");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(selectedUri, "image/*");
                startActivity(intent);
            }
        });

        SeekBar objSize = (SeekBar) findViewById(R.id.seek_obj_size);

        TextView sizeVal = (TextView) findViewById(R.id.obj_size);
        objSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Prefs.setObjectSize(getApplicationContext(), progress);
                sizeVal.setText(String.format("%.1f ",1 + progress / 100.0 * 19) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        objSize.setProgress(((int) (Prefs.getObjectSize(this))));

        View privacy = findViewById(R.id.privacy);
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://privacy-policies.appspot.com/motion_detector/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        stopCapture();

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopCapture();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCapture();

    }


}
