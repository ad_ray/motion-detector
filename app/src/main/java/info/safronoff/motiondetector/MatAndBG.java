package info.safronoff.motiondetector;

import org.opencv.core.Mat;

/**
 * Created by adray on 1/25/17.
 */

public class MatAndBG {
    Mat frame;
    Mat bg;

    public MatAndBG(Mat frame, Mat bg) {
        this.frame = frame;
        this.bg = bg;
    }

}
