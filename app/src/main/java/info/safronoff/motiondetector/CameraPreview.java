package info.safronoff.motiondetector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adray on 1/18/17.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.AutoFocusCallback {

    private Camera mCamera;

    Activity activity;

    ImageView bg;
    ImageView movingObject;

    MotionDetector detector;

    private SurfaceHolder mHolder;



    public void setCamera(Camera mCamera) {

        this.mCamera = mCamera;
        if(mCamera != null) {
            WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            int rotation = display.getRotation();
            int angle = 0;
            switch (rotation) {
                case Surface.ROTATION_90:
                    angle = 0;
                    break;
                case Surface.ROTATION_180:
                    angle = -90;
                    break;
                case Surface.ROTATION_270:
                    angle = 180;
                    break;
                default:
                    angle = 90;
                    break;
            }


            mCamera.setDisplayOrientation(angle);
            mHolder = getHolder();
            mHolder.addCallback(this);
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
    }

    public CameraPreview(Context context, Camera camera) {
        super(context);
        activity = (Activity) context;
        bg = (ImageView) activity.findViewById(R.id.background);
        movingObject = (ImageView) activity.findViewById(R.id.moving_object);
        ImageView frameToSave = (ImageView) activity.findViewById(R.id.frame_to_save);
        ImageView filter = (ImageView) activity.findViewById(R.id.filter);
        detector = new MotionDetector(context, bg, movingObject, frameToSave, camera, filter);
        setCamera(camera);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        setWillNotDraw(false);
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("Camera", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d("camera", "Error starting camera preview: " + e.getMessage());
        }
    }



    public int getOrientation() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();


        int orientation = display.getRotation();
        if(!isFrontal()) {
            orientation += 4;
        }
        return orientation;
    }

    private boolean isFrontal() {
        return false;
//        int id = Integer.parseInt( PreferenceManager.getDefaultSharedPreferences(getContext())
//                .getString(getContext().getResources().getString(R.string.camera_key), Camera.getNumberOfCameras() > 1 ? "1" : "0"));
//        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
//        android.hardware.Camera.getCameraInfo(id, info);
//        return info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;

    }









    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        CameraFrame frame = new CameraFrame(data, camera, getOrientation());
        detector.nextFrame(frame);

    }

//

    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }
}