package info.safronoff.motiondetector;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;

import com.google.gson.Gson;

/**
 * Created by adray on 3/23/17.
 */

public class Prefs {

    public static final String PREFS = "app_preferences";
    public static final String ROI = "ROI";
    public static final String BG = "BG";
    public static final String MOVE = "MOVE";
    public static final String CONTOUR = "CONTOUR";
    public static final String ROI_DATA = "ROI_DATA";
    public static final String OBJ_SIZE = "OBJ_SIZE";

    private static Point[] roiCache = null;


    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    public static float getObjectSize(Context context) {
        return getPrefs(context).getFloat(OBJ_SIZE, 1.0f);
    }

    public static void setObjectSize(Context context, float size) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat(OBJ_SIZE, size);
        editor.apply();
    }

    public static boolean isROIenabled(Context context) {
        return getPrefs(context).getBoolean(ROI, false);
    }

    public static boolean isBGEnabled(Context context) {
        return getPrefs(context).getBoolean(BG, true);
    }

    public static void setBGEnagled(Context context, boolean enabled) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(BG, enabled);
        editor.apply();
    }

    public static void setMovingEnagled(Context context, boolean enabled) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(MOVE, enabled);
        editor.apply();
    }

    public static boolean isMovingEnabled(Context context) {
        return getPrefs(context).getBoolean(MOVE, true);
    }

    public static void setContourEnagled(Context context, boolean enabled) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(CONTOUR, enabled);
        editor.apply();
    }

    public static boolean isContourEnabled(Context context) {
        return getPrefs(context).getBoolean(CONTOUR, true);
    }





    public static void setRoi(Context context, boolean enabled, DrawView roi) {
        Point[] points = new Point[4];
        for(int i = 0; i < roi.points.length; i ++) {
            Point p = roi.points[i];
            points[i] = new Point(
                    (p.x * 100) / roi.getWidth(),
                    (p.y * 100) / roi.getHeight());

        }
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(ROI, enabled);
        roiCache = points;
        Gson gson = new Gson();
        String data = gson.toJson(points);
        editor.putString(ROI_DATA, data);
        editor.apply();
    }

    public static Point[] getRoiData(DrawView roi) {
        if(roiCache == null) {
            Gson gson = new Gson();
            roiCache = gson.fromJson(getPrefs(roi.getContext()).getString(ROI_DATA, null), Point[].class);
        }
        return roiCache;
    }

    public static  Point[] getScaledRoiData(DrawView roi) {
        Point[] points = getRoiData(roi);
        if(points == null || points[0] == null)
            return null;

        Point[] newPoints = new Point[4];
        for(int i=0; i < 4; i ++) {
            Point p = points[i];
            newPoints[i] = new Point(p.x * roi.getWidth() / 100,
            (p.y * roi.getHeight()) / 100);
        }
        return newPoints;
    }

    public static org.opencv.core.Rect getRoiRect(DrawView roi) {
        org.opencv.core.Point p2 = new org.opencv.core.Point(0, 0);
        org.opencv.core.Point p1 = new org.opencv.core.Point(10000, 10000);
        android.graphics.Point[] points = Prefs.getRoiData(roi);
        for(android.graphics.Point p : points) {
            if(p.x > p2.x) {
                p2.x = p.x;
            }
            if(p.y > p2.y) {
                p2.y = p.y;
            }
            if(p.x < p1.x) {
                p1.x = p.x;
            }
            if(p.y < p1.y) {
                p1.y = p.y;
            }

        }
        return new org.opencv.core.Rect(p1, p2);
    }



}
